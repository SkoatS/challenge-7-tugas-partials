const { Router } = require("express");
const express = require("express");
const { user_game, user_game_history, user_game_biodata } = require ("./models");
const route = express.Router();


route.get("/", (req, res) => {
  res.json("ok");
});

route.post("/", (req, res) => {
  res.json("ok")
})


//  POST user_game
route.post("/login", async (req, res) => {
  const { body } = req;
  const users = await user_game.create(body);
  res.json(users);
});

// read(get) user_game
route.get("/login", async (req, res) => {
  const users = await user_game.findAll();
  res.json(users);
});


route.put("/login/:id", async (req, res) => {
  const { body } = req;
  const put_games = await user_game.update(body, {
    where: {
      id: req.params.id
    }
  });
  res.json(put_games);
});

route.delete("/login/:id", async (req, res) => {
  const d_games = await user_game.destroy({
    where: {
      id: req.params.id
    }
  });
  res.json(d_games);
});

// POST /history
route.post("/history", async (req, res) => {
  const { body } = req;
  const history = await user_game_history.create(body);
  res.json(history);
});

route.get("/history", async (req, res) => {
  const histories = await user_game_history.findAll({
    include: [
      {
        model: user_game,
        attributes: [ 'username', 'email' ]
      }
    ]
  });
  res.json(histories);
});

route.get("/history/:id", async (req, res) => {
  const fHistories = await user_game_history.findOne({
    where: {
      id: req.params.id
    },
    include: [
      {
        model: user_game,
        attributes: [ 'username', 'email' ]
      },
      {
        model: user_game_biodata
      }
    ]
  });
  res.json(fHistories);
});

// POST /biodata
route.post("/biodata", async (req, res) => {
  const { body } = req;
  const biodatas = await user_game_biodata.create(body);
  res.json(biodatas);
});


route.get("/biodata", async (req, res) => {
  const Fbiodata = await user_game_biodata.findAll({
    include: [
      {
        model: user_game
      },
    ]
  });
  res.json(Fbiodata);
});

route.put("/biodata/:id", async (req, res) => {
  const { body } = req;
  const put_biodata = await user_game_biodata.update(body, {
    where: {
      id: req.params.id
    }
  });
  res.json(put_biodata);
});


module.exports = route;