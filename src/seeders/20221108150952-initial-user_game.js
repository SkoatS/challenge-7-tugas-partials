'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert(
      'user_games', [
        {
          id: 1,
          username: 'Bege Pahlawanku',
          email: 'pahlawan.bege@hotmail.com',
          password: 'loveBege123',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          username: 'Perdana Pahlawanku',
          email: 'pahlawan.perdana@hotmail.com',
          password: 'PerdanaNapi123',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          username: 'BambangX2',
          email: 'bambangNtaps@hotmail.com',
          password: 'bambino222',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ]
    )
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('user_games')
  }
};
