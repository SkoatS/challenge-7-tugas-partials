const express = require("express");
const { User } = require("./models")
const route = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");
const { Strategy: JWTStrategy, ExtractJwt } = require("passport-jwt")

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: "secret"
}, async (payload, done) => {
  try {
    if(new Date() > new Date(payload.accesTokenExpiredAt)) {
      throw new Error('Acces token Expired At')
    }

    const user = await User.findByPk(payload.id);

    return done(null, user);
  } catch (error) {
    return done(null, false, { message: error.message });
  }
}));

route.get("/", (req, res) => {
  res.json({ success: "ok" });
});

route.post("/login", async (req, res, next) => {
  try{
    const user = await User.authenticate(req.body);
    const today = new Date();
    const accesTokenExpiredAt = today.setHours(today.getHours() + 2);
    res.json({
      accesToken: jwt.sign({ 
        id: user.id,
        username: user.username,
        accesTokenExpiredAt
       }, "secret"),
       accesTokenExpiredAt
    });
  } catch(error) {
    next(error)
  }
  
});

const allowedRoles = (roles) => (req, res, next) => {
    if (roles.include(req.user.role)) {
      next();
    } else {
      res.status(401).send("unathorized");
    }
  }

route.get("/user",
  passport.authenticate("jwt", { session: false }), 
  allowedRoles([ "user", "admin" ]),
  (req, res) => {
    res.json(req.user);
  });

route.get("/game",
  passport.authenticate("jwt", { session: false }), 
  allowedRoles([ "user", "admin" ]),
  (req, res) => {
    res.json(req.user);
  });

module.exports = route;